# DKX/Extends

Extends from typescript

## Installation

```bash
$ npm install --save @dkx/extend
```

## Usage

```typescript
import {extend} from '@dkx/extend';

extend(ChildObject, ParentObject);
```
