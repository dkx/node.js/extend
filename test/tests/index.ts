import {expect} from 'chai';

import {extend} from '../../src';


describe('extend()', () => {

	it('should extend class', () => {
		class Parent {}
		class Child {}

		extend(Child, Parent);

		const child = new Child;

		expect(child).to.be.an.instanceOf(Child);
		expect(child).to.be.an.instanceOf(Parent);
	});

});
